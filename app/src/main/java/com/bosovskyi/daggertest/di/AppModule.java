package com.bosovskyi.daggertest.di;

import com.bosovskyi.daggertest.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by boss1088 on 2/13/17.
 */

@Module
public class AppModule {

    private final App mContext;

    public AppModule(App context) {
        mContext = context;
    }

    @Provides
    @Singleton
    App provideContext() {
        return mContext;
    }
}
