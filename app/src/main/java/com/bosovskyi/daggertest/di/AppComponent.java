package com.bosovskyi.daggertest.di;

import com.bosovskyi.daggertest.App;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by boss1088 on 2/13/17.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);
}
