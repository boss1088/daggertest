package com.bosovskyi.daggertest;

import android.app.Application;

import com.bosovskyi.daggertest.di.AppComponent;
import com.bosovskyi.daggertest.di.AppModule;

/**
 * Created by boss1088 on 2/13/17.
 */

public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appmodule(new AppModule(this))
                .build();
        mAppComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
